select * from schedule;


-- to_char
-- 아래는 ORA-01481: 숫자 형식 모델이 부적합합니다 에러가 발생
-- sche_date는 yyyymmdd 형식인데, 오라클에서 이를 Number 형식으로 인식한다.
-- 숫자를 'YYYY-MM-DD'형식으로 변경하려고하니 에러가 발생한다.
select to_char(sche_date, 'YYYY-MM-DD') from schedule;

-- to_date
-- 아래는 정상작동한다.
-- 오라클에서 sche_date을 날짜로 인식한다.
select to_date(sche_date, 'YYYY-MM-DD') from schedule;

-- 결과
-- to_date로 날짜로 변경해준 후에, to_char을 사용하면 된다.
select to_char(to_date(sche_date, 'YYYY-MM-DD')) from schedule;